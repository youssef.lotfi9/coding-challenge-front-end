import React, { useEffect } from "react";
import Product from "../components/product/Product";
import { useDispatch, useSelector } from "react-redux";
import { getProductsAction } from "../redux/actions/productsActions";

const Home = () => {
  const { products } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsAction(products));
  }, []);

  return (
    <div>
      {products.products.isLoading ? (
        <h1>Loading please wait...</h1>
      ) : (
        <Product products={products} />
      )}
    </div>
  );
};

export default Home;
