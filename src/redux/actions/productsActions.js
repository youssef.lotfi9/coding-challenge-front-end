import axios from "axios";
import PRODUCTS from "../constants";

export const getProductsAction = (store) => async (dispatch) => {
  dispatch({
    type: PRODUCTS.LOAD,
    isLoading: true,
    isError: false,
  });

  try {
    const { data } = await axios.get("https://fakestoreapi.com/products/");
    dispatch({
      type: PRODUCTS.LOAD_SUCCESS,
      isLoading: false,
      products: data,
      isError: false,
    });
  } catch (e) {
    console.error(e);
    dispatch({
      type: PRODUCTS.LOAD_FAILED,
      products: [],
      isError: true,
      isLoading: false,
    });
  }
};
