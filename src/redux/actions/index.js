import PRODUCTS from "../constants";

export const addItem = (product) => {
  return {
    type: PRODUCTS.ADD_ITEM,
    payload: product,
  };
};

export const decreaseProductQuantity = (product) => {
  return {
    type: PRODUCTS.DECREASE_PRODUCT_QUANTITY,
    payload: product,
  };
};

export const removeItem = (product) => {
  return {
    type: PRODUCTS.REMOVE_ITEM,
    payload: product,
  };
};
