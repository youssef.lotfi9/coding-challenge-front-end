import PRODUCTS from "../constants";

const cart = [];

const cartItems = (state = cart, action) => {
  switch (action.type) {
    case PRODUCTS.ADD_ITEM:
      if (action.payload != null && action.payload.id != null) {
        const existingProduct = state.find(
          (product) => product.id === action.payload.id
        );

        if (existingProduct) {
          return state.map((product) => {
            if (product.id === action.payload.id) {
              return {
                ...product,
                quantity: product.quantity + 1,
              };
            }
            return product;
          });
        } else {
          const newProduct = {
            title: action.payload.title,
            id: action.payload.id,
            description: action.payload.description,
            rating: action.payload.rating,
            image: action.payload.image,
            price: action.payload.price,
            quantity: 1,
          };
          return [...state, newProduct];
        }
      }
    case PRODUCTS.DECREASE_PRODUCT_QUANTITY:
      const existingProduct2 = state.find(
        (product) => product.id === action.payload.id
      );

      if (existingProduct2.quantity === 1) {
        return (state = state.filter((x) => {
          return x.id !== action.payload.id;
        }));
      } else {
        return state.map((product) => {
          if (product.id === action.payload.id) {
            return {
              ...product,
              quantity: product.quantity - 1,
            };
          }
          return product;
        });
      }
    case PRODUCTS.REMOVE_ITEM:
      return (state = state.filter((x) => {
        return x.id !== action.payload.id;
      }));
    default:
      return state;
  }
};

export default cartItems;
