import PRODUCTS from "../constants";

const initialState = {
  products: [],
  isLoading: false,
  isError: false,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTS.LOAD:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case PRODUCTS.LOAD_SUCCESS:
      return {
        ...state,
        products: action.products,
        isLoading: false,
      };

    case PRODUCTS.LOAD_FAILED:
      return {
        ...state,
        products: action.products,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default productsReducer;
