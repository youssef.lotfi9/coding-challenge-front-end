import cartItemsReducer from "./cartItemsReducer";
import productsReducer from "./productsReducer";

import { combineReducers } from "redux";

const rootReducers = combineReducers({
  cartItems: cartItemsReducer,
  products: productsReducer,
});

export default rootReducers;
