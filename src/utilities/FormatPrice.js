export const formattedPrice = (totalPrice) =>
  totalPrice.toLocaleString("en-US", {
    style: "currency",
    currency: "MAD",
  });
