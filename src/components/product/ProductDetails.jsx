import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { addItem, decreaseProductQuantity } from "../../redux/actions/index";
import { Rate } from "rsuite";

const ProductDetail = () => {
  const proid = useParams();
  const [cartBtn, setCartBtn] = useState("Add to Cart");
  const product = useSelector((store) =>
    store.products.products.filter((x) => x.id == proid.id)
  );
  const dispatch = useDispatch();
  const handleCart = (product) => {
    if (cartBtn === "Add to Cart") {
      dispatch(addItem(product));
      setCartBtn("Remove from Cart");
    } else {
      dispatch(decreaseProductQuantity(product));
      setCartBtn("Add to Cart");
    }
  };

  const redirectIfProductNotFound = () => {
    if (proid === null || proid === undefined) {
      const timeout = setTimeout(() => {
        window.location.replace("/");
      }, 5000);

      return () => clearTimeout(timeout);
    }
  };
  return (
    <>
      {product === [] || product.length === 0 ? (
        <h1>
          Product not found : Redirect to home page
          {redirectIfProductNotFound()}
        </h1>
      ) : (
        <div className="container my-5 py-3">
          <div className="row">
            <div className="col-md-6 d-flex justify-content-center mx-auto product">
              <img
                src={product[0].image}
                alt={product[0].title}
                height="400px"
              />
            </div>
            <div className="col-md-6 d-flex flex-column justify-content-center">
              <h1 className="display-5 fw-bold">{product[0].title}</h1>
              <hr />
              <h2>{product[0].category}</h2>
              <p className="lead">{product[0].description}</p>
              <Rate readOnly value={product[0].rating.rate} allowHalf />
              <button
                onClick={() => handleCart(product[0])}
                className="btn btn-outline-primary my-5"
              >
                {cartBtn}
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ProductDetail;
