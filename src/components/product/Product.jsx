import React from "react";
import { NavLink } from "react-router-dom";
import { Rate } from "rsuite";

const Product = ({ products, isLoading }) => {
  const cardItem = (item) => {
    return (
      <div className="card my-5 py-4" key={item.id} style={{ width: "18rem" }}>
        <img
          src={item.image}
          className="card-img-top"
          alt={item.title}
          style={{
            height: "200px",
            float: "right",
            padding: "10px",
          }}
        />
        <div className="card-body text-center">
          <h5 className="card-title">{item.title}</h5>
          <p className="lead">${item.price}</p>
          <Rate readOnly value={item.rating.rate} allowHalf />
          <NavLink
            to={`/products/${item.id}`}
            className="btn btn-outline-primary"
          >
            Buy Now
          </NavLink>
        </div>
      </div>
    );
  };

  return (
    <div>
      <div className="container py-5">
        <div className="row">
          <div className="col-12 text-center">
            <h1>Products</h1>
            <hr />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row justify-content-around">
          {isLoading ? <h1>Loading...</h1> : products.products.map(cardItem)}
        </div>
      </div>
    </div>
  );
};

export default Product;
