const EmptyCart = () => {
  return (
    <div className="empty-cart-container">
      <div className="container py-4">
        <div className="row">
          <h3>Your Cart is Empty</h3>
        </div>
      </div>
    </div>
  );
};

export default EmptyCart;
