import React from "react";
import { useSelector } from "react-redux";
import "./Cart.scss";
import EmptyCart from "./EmptyCart";
import CartItems from "./CartItems";
import { formattedPrice } from "../../utilities/FormatPrice";
import { NavLink } from "react-router-dom";

const Cart = () => {
  const state = useSelector((state) => state.cartItems);

  const totalPrice = state.reduce((total, item) => {
    return total + item.price * item.quantity;
  }, 0);

  return (
    <React.Fragment>
      {state.length === 0 ? (
        <EmptyCart />
      ) : (
        state.map((cartItem) => <CartItems cartItem={cartItem} />)
      )}
      <div className="container">
        <h4 className="row mb-5 w-25 mx-auto">
          Total price: {formattedPrice(totalPrice)}
        </h4>

        <div className="row">
          <NavLink
            to="/checkout"
            className="btn btn-outline-primary mb-5 w-25 mx-auto disabled"
          >
            Proceed To checkout
          </NavLink>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Cart;
