import { useDispatch } from "react-redux";
import {
  addItem,
  decreaseProductQuantity,
  removeItem,
} from "../../redux/actions";
import { formattedPrice } from "../../utilities/FormatPrice";

const CartItems = ({ cartItem }) => {
  const dispatch = useDispatch();

  const handleClose = (item) => {
    dispatch(removeItem(item));
  };

  const handleIncrease = (product) => {
    dispatch(addItem(product));
  };

  const handleDecrease = (product) => {
    dispatch(decreaseProductQuantity(product));
  };

  const price = cartItem.price;
  const priceXQuantity = cartItem.price * cartItem.quantity;
  return (
    <div className="container" key={cartItem.id}>
      <div className="cart-container">
        <button
          onClick={() => handleClose(cartItem)}
          className="btn-close float-end"
          aria-label="Close"
        ></button>
        <div className="cart-items">
          <div className="cart-item">
            <img
              src={cartItem.image}
              alt={cartItem.title}
              height="200px"
              width="180px"
            />
          </div>
          <div className="cart-item">
            <h3>{cartItem.title}</h3>
            <p className="custom-lead">
              {price} x {cartItem.quantity} = {formattedPrice(priceXQuantity)}
            </p>
            <button onClick={() => handleIncrease(cartItem)}>+</button>
            {cartItem.quantity > 1 ? (
              <button onClick={() => handleDecrease(cartItem)}>-</button>
            ) : (
              <button className="disabled">-</button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartItems;
