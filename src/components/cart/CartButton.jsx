import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

const CartButton = () => {
  const state = useSelector((state) => state.cartItems);
  return (
    <>
      <NavLink to="/cart" className="btn btn-outline-primary ms-2" id="cart">
        <span className="fa fa-shopping-cart me-1"></span> Cart ({state.length})
      </NavLink>
    </>
  );
};

export default CartButton;
