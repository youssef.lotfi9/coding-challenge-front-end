import React from "react";
import CartBtn from "../cart/CartButton";
import { Link } from "react-router-dom";
import "./navbar.scss";

function Navbar() {
  return (
    <header className="header">
      <div className="header__content">
        <Link to="/" className="header__content__logo">
          Navbar
        </Link>
        <nav className="header__content__nav">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/store">Store</Link>
            </li>

            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </nav>

        <CartBtn />
      </div>
    </header>
  );
}

export default Navbar;
