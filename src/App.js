import { Route, Routes } from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import Home from "./pages/Home";
import Store from "./pages/Store";
import { About } from "./pages/About";
import Cart from "./components/cart/Cart";
import ProductDetail from "./components/product/ProductDetails";

function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/store" element={<Store />} />
        <Route path="/about" element={<About />} />
        <Route path="/cart" element={<Cart />} />
        <Route exact path="/products/:id" element={<ProductDetail />} />
      </Routes>
    </div>
  );
}

export default App;
