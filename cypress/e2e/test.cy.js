describe("imedia24 Store", () => {
  it("should display a welcome message on the homepage", () => {
    cy.visit("http://localhost:3000");

    cy.contains("Products");
  });

  describe("Add to Cart Flow", () => {
    it("should add item to cart and check cart count", () => {
      cy.visit("http://localhost:3000");
      cy.wait(1500);
      cy.contains("a", "Buy Now").click();
      cy.wait(1500);
      cy.contains("button", "Add to Cart").click();
      cy.wait(1500);
      cy.get("#cart").should("have.text", " Cart (1)");
    });
  });
});
