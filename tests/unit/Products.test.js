import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Product from "../components/Product";

describe("Product", () => {
  const products = {
    isLoading: false,
    products: [
      {
        id: 1,
        title: "Product 1",
        price: 10.99,
        image: "product1.jpg",
        rating: {
          rate: 3.5,
        },
      },
      {
        id: 2,
        title: "Product 2",
        price: 19.99,
        image: "product2.jpg",
        rating: {
          rate: 4.2,
        },
      },
    ],
  };

  it("renders product cards", () => {
    render(
      <BrowserRouter>
        <Product products={products} isLoading={false} />
      </BrowserRouter>
    );

    const product1Title = screen.getByText("Product 1");
    const product1Price = screen.getByText("$10.99");
    const product1Image = screen.getByAltText("Product 1");
    // const product1Rating = screen.getByRole("img", { name: "Rate" });

    const product2Title = screen.getByText("Product 2");
    const product2Price = screen.getByText("$19.99");
    const product2Image = screen.getByAltText("Product 2");
    // const product2Rating = screen.getByRole("img", { name: "Rate" });

    expect(product1Title).toBeInTheDocument();
    expect(product1Price).toBeInTheDocument();
    expect(product1Image).toBeInTheDocument();
    // expect(product1Rating).toBeInTheDocument();
    expect(product2Title).toBeInTheDocument();
    expect(product2Price).toBeInTheDocument();
    expect(product2Image).toBeInTheDocument();
    // expect(product2Rating).toBeInTheDocument();
  });

  it("renders loading state", () => {
    render(
      <BrowserRouter>
        <Product products={products} isLoading={true} />
      </BrowserRouter>
    );

    const loadingText = screen.getByText("Loading...");
    expect(loadingText).toBeInTheDocument();
  });
});
